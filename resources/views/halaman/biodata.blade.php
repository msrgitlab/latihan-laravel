<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biodata</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="" method="post">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="nama"> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="nama"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="G" value="Male"> Male <br>
        <input type="radio" name="G" value="Female"> Female <br>
        <input type="radio" name="G" value="Other"> Other <br><br>
        <label> Nationality</label> <br><br>
        <select name="N">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select> <br><br>
        <label> Language Spoken:</label> <br><br>
        <input type="checkbox" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language Spoken"> English <br>
        <input type="checkbox" name="Language Spoken"> Other <br><br>
        <label> Bio: </label> <br><br>
        <textarea name="Bio" cols="30" rows="9"></textarea> <br>

        <input type="submit" value="Sign Up"> <br><br><br>

    </form>
</body>
</html>